# LaSERia⅃

LaSERia⅃ is a collaborative project between [@305ph02u5](https://gitlab.com/305ph02u5) and [@ShubNigg](https://gitlab.com/ShubNigg) to create a USB device that permits the user to establish a serial connection over a laser beam.

## Hardware

The system is comprised of two parts: the transmitter and the receiver. The transmitter is a USB device that connects to the host computer and transmits data over a laser beam. The receiver is a photodiode that receives the laser beam and converts it back into a serial signal over USB.

Each PCB is designed by a different person. The transmitter is designed by [@ShubNigg](https://gitlab.com/ShubNigg) and the receiver is designed by [@305ph02u5](https://gitlab.com/305ph02u5).

### Transmitter

// TODO

### Receiver

// TODO

## Software

The system is made to be transparent for the host computer. The host computer should not be able to tell the difference between a USB serial port and a laser serial port. That's why the transmitter and receiver are USB devices that implement the USB CDC ACM class.

The only software that is needed is the firmware for the transmitter and receiver.